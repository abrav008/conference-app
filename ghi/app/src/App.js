import { useState, useEffect } from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";

function App(props) {
  const [ attendees, setAttendees ] = useState([]);
  const [ conferences, setConferences ] = useState([]);

  async function getAttendees() {
    const response = await fetch('http://localhost:8001/api/attendees/');
    if (response.ok) {
      const { attendees } = await response.json();
      setAttendees(attendees);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  async function getConferences() {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  useEffect(() => {
    getAttendees();
    getConferences();
  }, [])
  
  if (attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage conferences={conferences}/>} />
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm getConferences={getConferences} />} />
        </Route>
        <Route path="attendees">
          <Route index element={<AttendeesList attendees={attendees} />} />
          <Route path="new" element={<AttendConferenceForm conferences={conferences} getAttendees={getAttendees} />} />
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm conferences={conferences} />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
